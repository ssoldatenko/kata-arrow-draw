import javax.swing.*;
import java.awt.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseMotionListener;
import java.awt.geom.Dimension2D;
import java.awt.geom.Line2D;
import java.awt.geom.Point2D;
import java.awt.geom.Rectangle2D;
import java.util.ArrayList;
import java.util.List;

public class ArrowDraw extends JPanel {
    protected List<MyBox> boxes;
    protected List<Link> links;

    public static void main(String[] args) {
        showThisPanel(new ArrowDraw());
    }

    protected static void showThisPanel(ArrowDraw panel) {
        JFrame frame = new JFrame("ArrowDraw");
        frame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        frame.getRootPane().setLayout(new BorderLayout());
        frame.getRootPane().add(panel, BorderLayout.CENTER);
        frame.setBounds(500, 200, 800, 500);
        frame.setVisible(true);
    }

    public ArrowDraw() {
        boxes = new ArrayList<MyBox>();
        boxes.add(new MyBox(new Point(60, 50), new Dimension(60, 20)));
        boxes.add(new MyBox(new Point(150, 100), new Dimension(40, 30)));
        boxes.add(new MyBox(new Point(60, 250), new Dimension(60, 20)));
        boxes.add(new MyBox(new Point(170, 150), new Dimension(90, 30)));
        boxes.add(new MyBox(new Point(460, 150), new Dimension(40, 40)));
        boxes.add(new MyBox(new Point(360, 160), new Dimension(40, 100)));
        boxes.add(new MyBox(new Point(375, 60), new Dimension(50, 30)));

        links = new ArrayList<Link>();
        links.add(new Link(boxes.get(0), boxes.get(1)));
        links.add(new Link(boxes.get(2), boxes.get(3)));
        links.add(new Link(boxes.get(4), boxes.get(5)));
        links.add(new Link(boxes.get(4), boxes.get(6)));

        IvanDraga ivan = new IvanDraga(boxes.get(0));
        addMouseMotionListener(ivan);
        addMouseListener(ivan);
    }

    @Override
    public void paint(Graphics g) {
        Graphics2D g2 = (Graphics2D) g;
        g2.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);

        g.clearRect(getX(), getY(), getWidth(), getHeight());

        drawLinks(g2);
        drawBoxes(g2);
        drawArrows(g2);
    }

    protected void drawLinks(Graphics2D g2) {
        g2.setColor(Color.BLACK);
        for (Link link : links) {
            MyBox b1 = link.getStart();
            MyBox b2 = link.getEnd();
            Line2D.Double line = new Line2D.Double(b1.getCenterX(), b1.getCenterY(),
                    b2.getCenterX(), b2.getCenterY());
            g2.draw(line);
        }
    }

    protected void drawBoxes(Graphics2D g2) {
        g2.setColor(g2.getBackground());
        for (MyBox box : boxes) {
            Rectangle2D.Double rect = new Rectangle2D.Double(box.getX(), box.getY(), box.getW(), box.getH());
            g2.fill(rect);
        }
        g2.setColor(Color.BLACK);
        for (MyBox box : boxes) {
            Rectangle2D.Double rect = new Rectangle2D.Double(box.getX(), box.getY(), box.getW(), box.getH());
            g2.draw(rect);
        }
    }

    protected void drawArrows(Graphics2D g2) {
    }

    static class MyBox {
        private double x;
        private double y;
        private double w;
        private double h;

        public MyBox(Point2D point, Dimension2D size) {
            x = point.getX();
            y = point.getY();
            w = size.getWidth();
            h = size.getHeight();
        }

        public double getCenterX() {
            return x + (w / 2);
        }

        public double getCenterY() {
            return y + (h / 2);
        }

        public double getX() {
            return x;
        }

        public void setX(double x) {
            this.x = x;
        }

        public double getY() {
            return y;
        }

        public void setY(double y) {
            this.y = y;
        }

        public double getW() {
            return w;
        }

        public double getH() {
            return h;
        }
    }

    static class Link {
        private MyBox start;
        private MyBox end;

        Link(MyBox start, MyBox end) {
            this.start = start;
            this.end = end;
        }

        public MyBox getStart() {
            return start;
        }

        public MyBox getEnd() {
            return end;
        }
    }

    /**
     * Класс реализует возможность перетаскивания прямоугольников мышью.
     */
    private class IvanDraga extends MouseAdapter implements MouseMotionListener {
        private MyBox box;
        private boolean isDragged = false;
        private double mouseDeltaX;
        private double mouseDeltaY;

        private IvanDraga(MyBox b) {
            box = b;
        }

        @Override
        public void mouseDragged(MouseEvent e) {
            if (!isDragged) {
                mouseDeltaX = box.getX() - e.getX();
                mouseDeltaY = box.getY() - e.getY();
                isDragged = true;
            }
            box.setX(e.getX() + mouseDeltaX);
            box.setY(e.getY() + mouseDeltaY);
            repaint();
        }

        @Override
        public void mouseMoved(MouseEvent e) {
            isDragged = false;
        }

        @Override
        public void mousePressed(MouseEvent e) {
            for (MyBox b : boxes) {
                Rectangle2D r = new Rectangle.Double(b.getX(), b.getY(), b.getW(), b.getH());
                if (r.contains(e.getX(), e.getY())) {
                    box = b;
                    break;
                }
            }
        }
    }
}
