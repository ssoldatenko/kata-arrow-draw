import java.awt.*;
import java.awt.geom.Path2D;
import java.awt.geom.Point2D;

public class ArrowDraw2 extends ArrowDraw {
    public static void main(String[] args) {
        showThisPanel(new ArrowDraw2());
    }

    @Override
    protected void drawArrows(Graphics2D g2) {
        Color fillColor = new Color(0xE0, 0, 0xE0);
        Color outlineColor = Color.BLACK;

        for (Link link : links) {
            int al = 15; //arrow len
            int ah = 10; // arrow height

            MyBox b1 = link.getStart();
            MyBox b2 = link.getEnd();

            double deltaX = b2.getCenterX() - b1.getCenterX();
            double deltaY = b2.getCenterY() - b1.getCenterY();
            double len = Math.sqrt(deltaX * deltaX + deltaY * deltaY);
            if (len == 0) {
                // В случае совпадения центров прямоугольников, направление стрелки не
                // определено. Поэтому не будет рисовать стрелку для таких случаев.
                continue;
            }

            // Коофиценты для определения проекций на оси x и y.
            // Положительный kx означает, что значение X в конце линии больше значения X в начале
            // Отрицательный kx - наоборот.
            // Если линия вертикальная (направленная вниз или вверх), то kx == 0
            // То же и для ky, но только для оси Y.
            double kx = deltaX / len;
            double ky = deltaY / len;

            // s1 - точка выхода прямой из первого прямоугольника
            // s2 - точка входа прямой во второй прямоугольник (остриё стрелки)
            // s3 - точка отстоящая от острия на al (основание стрелки)
            // s4, s5 - концы основания стрелки.

            Point2D s1 = getExitPoint(b1, kx, ky);
            Point2D s2 = getExitPoint(b2, -kx, -ky);
            Point2D s3 = new Point2D.Double(s2.getX() - (al * kx), s2.getY() - (al * ky));
            Point2D s4 = new Point2D.Double(s3.getX() + (ah / 2 * ky), s3.getY() - (ah / 2 * kx));
            Point2D s5 = new Point2D.Double(s3.getX() - (ah / 2 * ky), s3.getY() + (ah / 2 * kx));

            Path2D.Double arrow = new Path2D.Double();
            arrow.moveTo(s2.getX(), s2.getY());
            arrow.lineTo(s4.getX(), s4.getY());
            arrow.lineTo(s5.getX(), s5.getY());
            arrow.lineTo(s2.getX(), s2.getY());

            g2.setColor(fillColor);
            g2.fill(arrow);

            g2.setColor(outlineColor);
            g2.draw(arrow);
        }
    }

    private Point2D getExitPoint(MyBox box, double kx, double ky) {
        // Луч исходящий из центра прямоугольника пересечет как минимум одну из
        // сторон прямоугольника (две - если пройдет через один из углов)
        // Направление можно определить по знакам kx и ky.
        // Здесь мы последовательно проверяем пересечения с каждой из сторон
        // и возвращаем найденую точку пересечения.
        // Для расчета принимаем центр прямоугольника за начало координат.

        double x1 = -box.getW() / 2;
        double x2 = box.getW() / 2;
        double y1 = -box.getH() / 2;
        double y2 = box.getH() / 2;

        // Левая вертикальная сторона.
        // x = -w/2, y = [-h/2; h/2] (При kx == 0 прямая не пересекается с
        // левой или правой стороной. Поэтому строгое меньше)
        if (kx < 0) {
            // a, b - катеты, c - гипотенуза
            // c * kx == a
            // c * ky == b
            double a = box.getW() / 2;
            double c = a / -kx;
            double b = c * ky;
            if (b >= y1 && b <= y2) {
                return new Point2D.Double(box.getCenterX() - a, box.getCenterY() + b);
            }
        }

        // Правая сторона:
        // x = w/2, y = [-h/2; h/2]
        if (kx > 0) {
            double a = box.getW() / 2;
            double c = a / kx;
            double b = c * ky;
            if (b >= y1 && b <= y2) {
                return new Point2D.Double(box.getCenterX() + a, box.getCenterY() + b);
            }
        }

        // x = [-w/2; w/2], y = -h/2
        if (ky < 0) {
            double a = box.getH() / 2;
            double c = a / -ky;
            double b = c * kx;
            if (b >= x1 && b <= x2) {
                return new Point2D.Double(box.getCenterX() + b, box.getCenterY() - a);
            }
        }

        // x = [-w/2; w/2], y = h/2
        if (ky > 0) {
            double a = box.getH() / 2;
            double c = a / ky;
            double b = c * kx;
            if (b >= x1 && b <= x2) {
                return new Point2D.Double(box.getCenterX() + b, box.getCenterY() + a);
            }
        }
        throw new IllegalArgumentException("Both kx and ky was 0");
    }
}
