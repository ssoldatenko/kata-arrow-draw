
Draw Arrow Code Kata
====================

ArrowDraw.java source file contains simple programm drawing few rectangles and links between them.
Drawing code is very simple. It draws lines betweeen centers of rectangles, and rectangles over it.

You should create code to draw arrows at the end of links.

See arrow-draw.png

ArrowDraw2.java contains my solution.

Ката разработчика Рисование стрелок
===================================

В файле ArrowDraw.java находится простая программа рисующая несколько прямоугольников и линии между ними.
Код рисования очень простой. Рисуем линии между центрами прямоугольников, а поверх них рисуем прямоугольники.

Ваша задача создать код, который нарисовал бы стрелочки в коцах линий.

См. arrow-draw.png

В файле ArrowDraw2.java моё решение.